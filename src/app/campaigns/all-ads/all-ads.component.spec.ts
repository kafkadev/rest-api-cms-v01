import { TestBed, inject } from '@angular/core/testing';

import { AllAdsComponent } from './all-ads.component';

describe('a all-ads component', () => {
	let component: AllAdsComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AllAdsComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([AllAdsComponent], (AllAdsComponent) => {
		component = AllAdsComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});