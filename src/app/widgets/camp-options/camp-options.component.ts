import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { AdmService } from '../../_services/adm.service';
@Component({
	selector: 'camp-options',
	templateUrl: 'camp-options.component.html'
})

export class CampOptionsComponent implements OnInit, AfterViewInit {
	@Input("camp_data") campaign: any;

	constructor(private adminService: AdmService) { }

	ngOnInit() { }
	ngAfterViewInit() { }

	formUpdate(event) {
		console.log(event.value);
	}

	updateCampaign(formData: any) {
		this.adminService.updateCampaign(formData.value).subscribe(resp => {
			console.log(resp);
		}, error => {
			console.log(error);
		});
	}
}