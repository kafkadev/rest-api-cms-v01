import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { AdministratorComponent } from './administrator.component';
import { SettingsComponent } from './settings/settings.component';
const routes: Routes = [
  {
    path: '',
    component: AdministratorComponent,
    data: {
      title: 'Administrator'
    }
  },
  {
    path: 'settings',
    component: SettingsComponent,
    data: {
      title: 'Settings'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministratorRoutingModule {}


