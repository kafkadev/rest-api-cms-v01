import { Component, OnInit } from '@angular/core';
import { AdmService } from '../_services/adm.service';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { Observable } from 'rxjs/Observable';
@Component({
	selector: 'app-campaigns',
	templateUrl: './campaigns.component.html',
	styleUrls: ['./campaigns.component.css']
})
export class CampaignsComponent implements OnInit {
	campaigns: any;
	current_Page: number = 0;
	pagesModel: any;
	constructor(private adminService: AdmService) {
		this.getCampaigns();

	}


	getCampaigns() {
		this.adminService.getAllCampaigns(this.current_Page).subscribe(resp => {
			this.pagesModel = this.adminService.getPaginationData(resp);
			this.campaigns = resp.data;
		},
			error => {
				console.log(error);
			});
	}

	ngOnInit() {
	}



	public pageChanged(event: any): void {
		this.current_Page = event.page;
		this.getCampaigns();
		//console.log('Page changed to: ' + event);
		//console.log('Number items per page: ' + event.itemsPerPage);
	}

}