import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Sidebar } from './sidebar.model';

@Injectable()
export class SidebarService {

	constructor(private http: Http) { }

	getList(): Observable<Sidebar[]> {
		return this.http.get('/api/list').map(res => res.json() as Sidebar[]);
	}
}