import { TestBed, inject } from '@angular/core/testing';

import { CamAdsComponent } from './cam-ads.component';

describe('a cam-ads component', () => {
	let component: CamAdsComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				CamAdsComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([CamAdsComponent], (CamAdsComponent) => {
		component = CamAdsComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});