import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard, AdmGuard } from './_guards/index';
import { AppComponent } from './app.component';
//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { AdministratorLayoutComponent } from './layouts/administrator-layout.component';
import { CustomerLayoutComponent } from './layouts/customer-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

export const routes: Routes = [
  {
    path: 'adm',
    component: AdministratorLayoutComponent,
    canActivate: [AdmGuard],
    data: {
      title: 'Administrator Dashboard'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './administrator/administrator.module#AdministratorModule'
        // loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
    ]
  },
  {
    path: 'cust',
    component: CustomerLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Customer Dashboard'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './customer/customer.module#CustomerModule'
      },
    ]
  },
  {
    path: 'camps',
    component: CustomerLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Campaigns Dashboard'
    },
    loadChildren: './campaigns/campaigns.module#CampaignsModule'
  },
  {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: './pages/pages.module#PagesModule',
      }
    ]
  },
  {
    path: '',
    redirectTo: '/camps',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/pages/login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


/*  
{
    path: '',
    component: SimpleLayoutComponent,
  }
*/