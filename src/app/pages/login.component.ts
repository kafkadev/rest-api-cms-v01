import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Login } from './shared/login.model';
//import { LoginService } from './shared/login.service';
import { AuthenticationService } from '../_services/index';
@Component({
	selector: 'login',
	templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
	login: Login[] = [];
	model: any = {};
	loading = false;
	returnUrl: string;
	constructor(
		private loginService: AuthenticationService,
		private route: ActivatedRoute,
		private router: Router
	) { }

	ngOnInit() {
	}

	submitLogin() {
		this.loading = true;
		this.loginService.login(this.model.username, this.model.password).subscribe(
			res => {
				let currentUserJson: any = localStorage.getItem('currentToken');
				//	let currentUserJson: any = localStorage.getItem('currentToken');
				//	var currentUserParse = JSON.parse(currentUserJson);
				//	this.parseJwt(currentUserParse.jwt);
				//	console.log(this.model);
				//	console.log(currentUserJson);
				if (currentUserJson !== "" || currentUserJson !== null || currentUserJson.lenght > 0) {
					this.router.navigate(['camps']);
				}
				//	this.router.navigate(['camps/dashboard']);
			},
			error => {
				//this.alertService.error(error);
				//this.loading = false;
			});

	}

	parseJwt(token) {
		var base64Url = token.split('.')[1];
		var base64 = base64Url.replace('-', '+').replace('_', '/');
		var parseJwte = JSON.parse(window.atob(base64));
		localStorage.setItem('currentData', JSON.stringify(parseJwte.data));
		console.log(parseJwte);


	};

}

/*    
this.authenticationService.login(this.model.username, this.model.password)
.subscribe(
data => {
this.router.navigate([this.returnUrl]);
},
error => {
//this.alertService.error(error);
this.loading = false;
});
*/