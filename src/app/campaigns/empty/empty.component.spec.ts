import { TestBed, inject } from '@angular/core/testing';

import { EmptyComponent } from './empty.component';

describe('a empty component', () => {
	let component: EmptyComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				EmptyComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([EmptyComponent], (EmptyComponent) => {
		component = EmptyComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});