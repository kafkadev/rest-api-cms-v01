webpackJsonp([1,8],{

/***/ 686:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(38);
var common_1 = __webpack_require__(8);
var administrator_component_1 = __webpack_require__(689);
var administrator_routing_1 = __webpack_require__(696);
var settings_component_1 = __webpack_require__(690);
var AdministratorModule = (function () {
    function AdministratorModule() {
    }
    return AdministratorModule;
}());
AdministratorModule = __decorate([
    core_1.NgModule({
        imports: [
            http_1.HttpModule,
            common_1.CommonModule,
            administrator_routing_1.AdministratorRoutingModule
        ],
        declarations: [administrator_component_1.AdministratorComponent, settings_component_1.SettingsComponent]
    })
], AdministratorModule);
exports.AdministratorModule = AdministratorModule;
//# sourceMappingURL=G:/laragon/www/angular/src/administrator.module.js.map

/***/ }),

/***/ 689:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var adm_service_1 = __webpack_require__(24);
var AdministratorComponent = (function () {
    function AdministratorComponent(adminService) {
        this.adminService = adminService;
    }
    AdministratorComponent.prototype.ngOnInit = function () { };
    return AdministratorComponent;
}());
AdministratorComponent = __decorate([
    core_1.Component({
        selector: 'app-administrator',
        template: __webpack_require__(701),
        styles: [__webpack_require__(699)],
        providers: [adm_service_1.AdmService]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof adm_service_1.AdmService !== "undefined" && adm_service_1.AdmService) === "function" && _a || Object])
], AdministratorComponent);
exports.AdministratorComponent = AdministratorComponent;
var _a;
//# sourceMappingURL=G:/laragon/www/angular/src/administrator.component.js.map

/***/ }),

/***/ 690:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var SettingsComponent = (function () {
    function SettingsComponent() {
    }
    SettingsComponent.prototype.ngOnInit = function () { };
    return SettingsComponent;
}());
SettingsComponent = __decorate([
    core_1.Component({
        selector: 'settings',
        template: __webpack_require__(702)
    })
], SettingsComponent);
exports.SettingsComponent = SettingsComponent;
//# sourceMappingURL=G:/laragon/www/angular/src/settings.component.js.map

/***/ }),

/***/ 696:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(14);
var administrator_component_1 = __webpack_require__(689);
var settings_component_1 = __webpack_require__(690);
var routes = [
    {
        path: '',
        component: administrator_component_1.AdministratorComponent,
        data: {
            title: 'Administrator'
        }
    },
    {
        path: 'settings',
        component: settings_component_1.SettingsComponent,
        data: {
            title: 'Settings'
        }
    }
];
var AdministratorRoutingModule = (function () {
    function AdministratorRoutingModule() {
    }
    return AdministratorRoutingModule;
}());
AdministratorRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forChild(routes)],
        exports: [router_1.RouterModule]
    })
], AdministratorRoutingModule);
exports.AdministratorRoutingModule = AdministratorRoutingModule;
//# sourceMappingURL=G:/laragon/www/angular/src/administrator.routing.js.map

/***/ }),

/***/ 699:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(32)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 701:
/***/ (function(module, exports) {

module.exports = "<p>administrator component</p>\r\n"

/***/ }),

/***/ 702:
/***/ (function(module, exports) {

module.exports = "<div class=\"settings\"> Hello SettingsComponent! </div>"

/***/ })

});
//# sourceMappingURL=1.chunk.js.map