import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { staticsData } from '../shared/statics-data';
@Injectable()
export class UploadServiceService {

 constructor(private http: Http) { }
    uploadImage(formData) {
        return this.http.post(staticsData.api_url + '/demo-return', formData).map((response: Response) => response.json());
    }

     getAdItem(camp_id : number, ad_id:number) {
        return this.http.get(staticsData.api_url + `/camp/${camp_id}/${ad_id}`).map((response: Response) => response.json());
    }
}