import { TestBed, inject } from '@angular/core/testing';

import { SettingsComponent } from './settings.component';

describe('a settings component', () => {
	let component: SettingsComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				SettingsComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([SettingsComponent], (SettingsComponent) => {
		component = SettingsComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});