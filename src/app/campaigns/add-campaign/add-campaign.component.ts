import { Component, OnInit } from '@angular/core';
import { AdmService } from '../../_services/adm.service';
@Component({
	selector: 'add-campaign',
	templateUrl: 'add-campaign.component.html'
})

export class AddCampaignComponent implements OnInit {
	formData: any;
	constructor(private adminService: AdmService) { }

	ngOnInit() {}

	formState(event) {
		this.formData = event.value;
		console.log(event.value);
	}

	submitPost() {
		this.adminService.createCampaign(this.formData).subscribe(resp => {
			console.log(resp);
		},
			error => {
				console.log(error);
			});
	}
}