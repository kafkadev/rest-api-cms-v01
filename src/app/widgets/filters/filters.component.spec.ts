import { TestBed, inject } from '@angular/core/testing';

import { FiltersComponent } from './filters.component';

describe('a filters component', () => {
	let component: FiltersComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				FiltersComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([FiltersComponent], (FiltersComponent) => {
		component = FiltersComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});