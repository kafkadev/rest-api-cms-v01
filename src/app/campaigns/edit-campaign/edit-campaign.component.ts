import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AdmService } from '../../_services/adm.service';
import { Router, ActivatedRoute } from '@angular/router';
import { staticsData } from '../../shared/statics-data';
declare var window: any;
declare var $: any;
@Component({
	selector: 'edit-campaign',
	templateUrl: 'edit-campaign.component.html'
})
export class EditCampaignComponent implements OnInit, AfterViewInit {
	campaign: any;
	camp_id: number;
	constructor(private adminService: AdmService, private route: ActivatedRoute) {
		this.camp_id = route.snapshot.params['camp_id'];
		this.adminService.getCampaign(this.camp_id)
			.subscribe(
			resp => {
				this.campaign = resp
				console.log(resp);
				
			},
			error => { console.log(error) }
			);
	}
	
	ngOnInit() {
		//console.log(this.campaign);
		//console.log(staticsData.currentUser);
	}

	ngAfterViewInit() {
		let arr = [
			"/assets/js/core/app.js",
			"/assets/js/pages/form_bootstrap_select.js",
			"/assets/js/pages/form_checkboxes_radios.js"
		]
		setTimeout(function () {
			window.jsLoad(arr);
		}, 2000);
	}


	formUpdate(event) {
		console.log(event.value);
	}

}