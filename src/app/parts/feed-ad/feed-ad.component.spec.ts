import { TestBed, inject } from '@angular/core/testing';

import { FeedAdComponent } from './feed-ad.component';

describe('a feed-ad component', () => {
	let component: FeedAdComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				FeedAdComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([FeedAdComponent], (FeedAdComponent) => {
		component = FeedAdComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});