import { Component, OnInit } from '@angular/core';
import { AdmService } from '../../_services/adm.service';
import { staticsData } from '../../shared/statics-data';
@Component({
	selector: 'all-ads',
	templateUrl: 'all-ads.component.html'
})

export class AllAdsComponent implements OnInit {
	all_items: any;
	pagesModel: any;
	current_Page: number = 0; 
	constructor(private adminService: AdmService) {
	}
	ngOnInit() {
	this.getCampaigns();
	}


		getCampaigns() {
			console.log(staticsData.currentUser);
			
		this.adminService.getAllItems(staticsData.currentUser.id, this.current_Page).subscribe(resp => {
			this.all_items = resp.data;
			this.pagesModel = this.adminService.getPaginationData(resp);

		},
			error => {
				console.log(error);
			});
	}


	public pageChanged(event: any): void {
		this.current_Page = event.page;
		this.getCampaigns();
	}

}