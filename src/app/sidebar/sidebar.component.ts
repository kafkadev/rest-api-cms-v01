import { Component, OnInit } from '@angular/core';
import { Sidebar } from './shared/sidebar.model';
import { SidebarService } from './shared/sidebar.service';
@Component({
	selector: 'sidebar',
	templateUrl: 'sidebar.component.html',
	providers: [SidebarService]
})

export class SidebarComponent implements OnInit {
	sidebar: Sidebar[] = [];

	constructor(private sidebarService: SidebarService) {}

	ngOnInit() {
	/*	this.sidebarService.getList().subscribe((res) => {
			this.sidebar = res;
		});*/
	}
}
