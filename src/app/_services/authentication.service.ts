import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthenticationService {
    constructor(private http: Http,	private router: Router) { }

    login(username: string, password: string) {
        return this.http.post('//adserver.kafkadev.agency/public/api/postapilogin', { email: username, password: password })
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                console.log(user);
                
                if (user && user.api_token) {

                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentToken', JSON.stringify(user.api_token));
                    localStorage.setItem('currentData', JSON.stringify(user));
                    return user;
                }
            });
    }

    loginPost(username: string, password: string) {
        let data = "email=" + username + "&password=" + password;
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post('//teniscim.herokuapp.com/user/login', data, options)
            .map((response: Response) => {
                console.log(response);
                let user = response.json();
                if (user) {
                    localStorage.setItem('currentData', JSON.stringify(user));
                }
                return user;
            });
    }


    logout() {
        // remove user from local storage to log user out
localStorage.removeItem('currentData');
localStorage.removeItem('currentToken');
	this.router.navigate(['pages/login']);
    }
}