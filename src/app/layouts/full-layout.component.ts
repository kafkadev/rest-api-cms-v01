import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {

  constructor() { }

  public disabled:boolean = false;
  public status:{isopen:boolean} = {isopen: false};

  public toggled(open:boolean):void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event:MouseEvent):void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit(): void {}
}



    /* 
import { Router, NavigationStart, ActivatedRoute} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { INCREMENT, DECREMENT, RESET, GUNCELLE } from '../ngrx/counter';

      title = 'app works!';
      parametre : any;
      counter: Observable<number>;

interface AppState {
  counter: number;
}

    declare var jQuery:any;
declare var $: any;
//const loadsScript = ['assets/js/core/app.js'];
    
     constructor(private adminService : AdmService,private store: Store<AppState>) { }


    this.adminService.getAllCourts()
    .subscribe(
    data => {
      console.log(data);

    //this.router.navigate([this.returnUrl]);

    },
    error => {
      console.log(error);

    //this.alertService.error(error);
    //this.loading = false;
    });
    */
   /*
   
         this.counter = store.select('counter');
              this.increment();
    	increment(){
    		this.store.dispatch({ type: INCREMENT });
        this.store.dispatch({
          type: GUNCELLE,
          payload : 'selamlar olsun'
       });
    	}

    	decrement(){
    		this.store.dispatch({ type: DECREMENT });
    	}

    	reset(){
    		this.store.dispatch({ type: RESET });
    	}*/