import { TestBed, inject } from '@angular/core/testing';

import { CamReportComponent } from './cam-report.component';

describe('a cam-report component', () => {
	let component: CamReportComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				CamReportComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([CamReportComponent], (CamReportComponent) => {
		component = CamReportComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});