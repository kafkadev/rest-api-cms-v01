import { NgModule, ApplicationRef, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { CampaignsComponent } from './campaigns.component';
import { CampaignsRoutingModule } from './campaigns.routing';
import { AddCampaignComponent } from './add-campaign/add-campaign.component';
import { AddItemComponent } from './add-item/add-item.component';
import { EditCampaignComponent } from './edit-campaign/edit-campaign.component';
import { EditItemComponent } from './edit-item/edit-item.component';
import { ImageAdComponent } from '../parts/image-ad/image-ad.component';
import { TextImageAdComponent } from '../parts/text-image-ad/text-image-ad.component';
import { FeedAdComponent } from '../parts/feed-ad/feed-ad.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { ToolsSidebarComponent } from '../widgets/tools-sidebar/tools-sidebar.component';
import { FiltersComponent } from '../widgets/filters/filters.component';
import { CampOptionsComponent } from '../widgets/camp-options/camp-options.component';
import { CamAdsComponent } from '../widgets/cam-ads/cam-ads.component';
import { CamReportComponent } from '../widgets/cam-report/cam-report.component';
import { FirstDisplayComponent } from '../widgets/first-display/first-display.component';
import { CampToolsComponent } from '../widgets/camp-tools/camp-tools.component';
import { AllAdsComponent } from './all-ads/all-ads.component';
import { EmptyComponent } from './empty/empty.component';

import { PaginationModule } from 'ngx-bootstrap';
@NgModule({
  imports: [
    HttpModule,
    CommonModule,
    CampaignsRoutingModule,
    FormsModule,
    PaginationModule.forRoot()
  ],
  declarations: [
    CampaignsComponent,
    AddCampaignComponent,
    AddItemComponent,
    EditCampaignComponent,
    EditItemComponent,
    ImageAdComponent,
    TextImageAdComponent,
    FeedAdComponent,
    SidebarComponent,
    ToolsSidebarComponent,
    FiltersComponent,
    CampOptionsComponent,
    CamAdsComponent,
    CamReportComponent,
    FirstDisplayComponent,
    CampToolsComponent,
    AllAdsComponent,
    EmptyComponent
  ],
  exports: [SidebarComponent, ToolsSidebarComponent],
  providers: [
  ]
})
export class CampaignsModule { }