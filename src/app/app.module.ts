import {NgModule,ApplicationRef} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { DatepickerModule } from 'ngx-bootstrap';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';

import { counterReducer } from './ngrx/counter';
import { baskaReduc } from './ngrx/baskaReduc';


import { AuthenticationService, AuthService, AdmService, UploadServiceService, TdFileService } from './_services/index';
import { AuthGuard, AdmGuard } from './_guards/index';
// Routing Module
import { AppRoutingModule } from './app.routing';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { AdministratorLayoutComponent } from './layouts/administrator-layout.component';
import { CustomerLayoutComponent } from './layouts/customer-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { CampaignsModule} from './campaigns/campaigns.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { ToolsSidebarComponent } from './widgets/tools-sidebar/tools-sidebar.component';

const appReducers: any = {
  counter: counterReducer,
  baska: baskaReduc
};

const appInitialState: any = {
  demo: 'demo data',

};

const StateStore: ModuleWithProviders = StoreModule.provideStore(appReducers);



@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    StateStore,
    HttpModule,
    FormsModule,
    CampaignsModule
  ],
  declarations: [
    FullLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    SimpleLayoutComponent,
    AdministratorLayoutComponent,
    CustomerLayoutComponent,
    AppComponent
  ],
  providers: [
    AuthenticationService,
    AuthService,
    AdmService,
    UploadServiceService,
    TdFileService,
    AdmGuard,
    AuthGuard,{
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }],
    exports: [],
  entryComponents: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  /*
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  */
}
