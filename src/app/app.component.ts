import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, RouteConfigLoadEnd, RoutesRecognized, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { INCREMENT, DECREMENT, RESET, GUNCELLE, allState } from './ngrx/baskaReduc';
//import "../assets/js/plugins/forms/styling/uniform.min.js";
//import "../assets/js/plugins/forms/selects/bootstrap_select.min.js";
declare var window: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  butunData: any = allState;
  parametre: any;
  ids: any;
  breadcrumbs: any;
  routeData: any = null
  route = new ActivatedRoute
  firstArr = [
    "/assets/js/plugins/forms/styling/uniform.min.js",
    "/assets/js/plugins/forms/selects/bootstrap_select.min.js",
    "/assets/js/core/app.js"

  ]
  constructor(private router: Router, private store: Store<any>) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        //console.log(this.butunData);

        window.jsLoad(this.firstArr);
        setTimeout(function () {
          //window.stringJs(deneme);
        }, 3000);
        //window.jsLoad();
      }
    });
  };

  ngOnInit() {
    window.jsLoad(this.firstArr);

  }

}


  /*store.select(state => state.baska).subscribe(data => {
      this.parametre = data
    })
    // console.log("butun Data", this.butunData);
      //console.log("butun AllState", allState);
    //store.select('feedsReducer').subscribe((data: AppState) => this.state = data );
    //store.select(state => state.feeds).do(feeds => this.feeds = feeds).subscribe();
    */