import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AdmService } from '../../_services/adm.service';


@Component({
	selector: 'filters',
	templateUrl: 'filters.component.html'
})

export class FiltersComponent implements OnInit, AfterViewInit {
	formData: any;
	rules: any;
	constructor(private adminService: AdmService) {}
	@Input("camp_data") campaign: any;
	@Input("camp_rules") camp_rules;

	ngOnInit() {
		this.rules = this.camp_rules;
	}
	ngAfterViewInit() {}

	formUpdate(event : NgForm) {
		this.formData = event.value
		console.log(this.formData);
	}

	submitAd() {
		this.adminService.updateRules(this.formData).subscribe(resp => {
			console.log(resp);
		}, error => {
			console.log(error);
		});
	}

}